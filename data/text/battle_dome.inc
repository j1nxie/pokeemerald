gBattleDomeOpponentPotentialText1::
	.string "The best candidate to be a champ!$"

gBattleDomeOpponentPotentialText2::
	.string "A sure-finalist team.$"

gBattleDomeOpponentPotentialText3::
	.string "A likely top-three finisher.$"

gBattleDomeOpponentPotentialText4::
	.string "A candidate to finish first.$"

gBattleDomeOpponentPotentialText5::
	.string "A team with top-class potential.$"

gBattleDomeOpponentPotentialText6::
	.string "The dark horse team this tournament.$"

gBattleDomeOpponentPotentialText7::
	.string "A better-than-average team.$"

gBattleDomeOpponentPotentialText8::
	.string "This tournament's average team.$"

gBattleDomeOpponentPotentialText9::
	.string "A team with average potential.$"

gBattleDomeOpponentPotentialText10::
	.string "A weaker-than-average team.$"

gBattleDomeOpponentPotentialText11::
	.string "A team looking for its first win.$"

gBattleDomeOpponentPotentialText12::
	.string "One win will make this team proud.$"

gBattleDomeOpponentPotentialText13::
	.string "Overall, a weak team.$"

gBattleDomeOpponentPotentialText14::
	.string "A team with very low potential.$"

gBattleDomeOpponentPotentialText15::
	.string "A team unlikely to win the tournament.$"

gBattleDomeOpponentPotentialText16::
	.string "The team most unlikely to win.$"

gBattleDomeOpponentPotentialText17::
	.string "The perfect, invincible superstar!$"

gBattleDomeOpponentStyleText1::
	.string "Willing to risk total disaster at times.$"

gBattleDomeOpponentStyleText2::
	.string "Skilled at enduring long battles.$"

gBattleDomeOpponentStyleText3::
	.string "Varies tactics to suit the opponent.$"

gBattleDomeOpponentStyleText4::
	.string "Has a tough winning pattern.$"

gBattleDomeOpponentStyleText5::
	.string "Occasionally uses a very rare move.$"

gBattleDomeOpponentStyleText6::
	.string "Uses startling and disruptive moves.$"

gBattleDomeOpponentStyleText7::
	.string "Constantly watches HP in battle.$"

gBattleDomeOpponentStyleText8::
	.string "Good at storing then loosing power.$"

gBattleDomeOpponentStyleText9::
	.string "Skilled at enfeebling foes.$"

gBattleDomeOpponentStyleText10::
	.string "Prefers tactics that rely on luck.$"

gBattleDomeOpponentStyleText11::
	.string "Attacks with a regal atmosphere.$"

gBattleDomeOpponentStyleText12::
	.string "Attacks with powerful, low-PP moves.$"

gBattleDomeOpponentStyleText13::
	.string "Skilled at enfeebling, then attacking.$"

gBattleDomeOpponentStyleText14::
	.string "Battles while enduring all attacks.$"

gBattleDomeOpponentStyleText15::
	.string "Skilled at upsetting foes emotionally.$"

gBattleDomeOpponentStyleText16::
	.string "Uses strong and straightforward moves.$"

gBattleDomeOpponentStyleText17::
	.string "Aggressively uses strong moves.$"

gBattleDomeOpponentStyleText18::
	.string "Battles while cleverly dodging attacks.$"

gBattleDomeOpponentStyleText19::
	.string "Skilled at using upsetting attacks.$"

gBattleDomeOpponentStyleText20::
	.string "Uses many popular moves.$"

gBattleDomeOpponentStyleText21::
	.string "Has moves for powerful combinations.$"

gBattleDomeOpponentStyleText22::
	.string "Uses high-probability attacks.$"

gBattleDomeOpponentStyleText23::
	.string "Aggressively uses spectacular moves.$"

gBattleDomeOpponentStyleText24::
	.string "Emphasizes offense over defense.$"

gBattleDomeOpponentStyleText25::
	.string "Emphasizes defense over offense.$"

gBattleDomeOpponentStyleText26::
	.string "Attacks quickly with strong moves.$"

gBattleDomeOpponentStyleText27::
	.string "Often uses moves with added effects.$"

gBattleDomeOpponentStyleText28::
	.string "Uses a well-balanced mix of moves.$"

gBattleDomeOpponentStyleTextUnused1::
	.string "This is sample message 1.$"

gBattleDomeOpponentStyleTextUnused2::
	.string "This is sample message 2.$"

gBattleDomeOpponentStyleTextUnused3::
	.string "This is sample message 3.$"

gBattleDomeOpponentStyleTextUnused4::
	.string "This is sample message 4.$"

gBattleDomeOpponentStatsText1::
	.string "Emphasizes HP and Attack.$"

gBattleDomeOpponentStatsText2::
	.string "Emphasizes HP and Defense.$"

gBattleDomeOpponentStatsText3::
	.string "Emphasizes HP and Speed.$"

gBattleDomeOpponentStatsText4::
	.string "Emphasizes HP and Sp. Attack.$"

gBattleDomeOpponentStatsText5::
	.string "Emphasizes HP and Sp. Defense.$"

gBattleDomeOpponentStatsText6::
	.string "Emphasizes Attack and Defense.$"

gBattleDomeOpponentStatsText7::
	.string "Emphasizes Attack and Speed.$"

gBattleDomeOpponentStatsText8::
	.string "Emphasizes Attack and Sp. Attack.$"

gBattleDomeOpponentStatsText9::
	.string "Emphasizes Attack and Sp. Defense.$"

gBattleDomeOpponentStatsText10::
	.string "Emphasizes Defense and Speed.$"

gBattleDomeOpponentStatsText11::
	.string "Emphasizes Defense and Sp. Attack.$"

gBattleDomeOpponentStatsText12::
	.string "Emphasizes Defense and Sp. Defense.$"

gBattleDomeOpponentStatsText13::
	.string "Emphasizes Speed and Sp. Attack.$"

gBattleDomeOpponentStatsText14::
	.string "Emphasizes Speed and Sp. Defense.$"

gBattleDomeOpponentStatsText15::
	.string "Emphasizes Sp. Attack and Sp. Defense.$"

gBattleDomeOpponentStatsText16::
	.string "Emphasizes HP.$"

gBattleDomeOpponentStatsText17::
	.string "Emphasizes Attack.$"

gBattleDomeOpponentStatsText18::
	.string "Emphasizes Defense.$"

gBattleDomeOpponentStatsText19::
	.string "Emphasizes Speed.$"

gBattleDomeOpponentStatsText20::
	.string "Emphasizes Sp. Attack.$"

gBattleDomeOpponentStatsText21::
	.string "Emphasizes Sp. Defense.$"

gBattleDomeOpponentStatsText22::
	.string "Neglects HP and Attack.$"

gBattleDomeOpponentStatsText23::
	.string "Neglects HP and Defense.$"

gBattleDomeOpponentStatsText24::
	.string "Neglects HP and Speed.$"

gBattleDomeOpponentStatsText25::
	.string "Neglects HP and Sp. Attack.$"

gBattleDomeOpponentStatsText26::
	.string "Neglects HP and Sp. Defense.$"

gBattleDomeOpponentStatsText27::
	.string "Neglects Attack and Defense.$"

gBattleDomeOpponentStatsText28::
	.string "Neglects Attack and Speed.$"

gBattleDomeOpponentStatsText29::
	.string "Neglects Attack and Sp. Attack.$"

gBattleDomeOpponentStatsText30::
	.string "Neglects Attack and Sp. Defense.$"

gBattleDomeOpponentStatsText31::
	.string "Neglects Defense and Speed.$"

gBattleDomeOpponentStatsText32::
	.string "Neglects Defense and Sp. Attack.$"

gBattleDomeOpponentStatsText33::
	.string "Neglects Defense and Sp. Defense.$"

gBattleDomeOpponentStatsText34::
	.string "Neglects Speed and Sp. Attack.$"

gBattleDomeOpponentStatsText35::
	.string "Neglects Speed and Sp. Defense.$"

gBattleDomeOpponentStatsText36::
	.string "Neglects Sp. Attack and Sp. Defense.$"

gBattleDomeOpponentStatsText37::
	.string "Neglects HP.$"

gBattleDomeOpponentStatsText38::
	.string "Neglects Attack.$"

gBattleDomeOpponentStatsText39::
	.string "Neglects Defense.$"

gBattleDomeOpponentStatsText40::
	.string "Neglects Speed.$"

gBattleDomeOpponentStatsText41::
	.string "Neglects Sp. Attack.$"

gBattleDomeOpponentStatsText42::
	.string "Neglects Sp. Defense.$"

gBattleDomeOpponentStatsText43::
	.string "Raises Pokémon in a well-balanced way.$"

gBattleDomeWinText1::
	.string "Let the battle begin!$"

gBattleDomeWinText2::
	.string "{STR_VAR_1} won using {STR_VAR_2}!$"

gBattleDomeWinText3::
	.string "{STR_VAR_1} became the champ!$"

gBattleDomeWinText4::
	.string "{STR_VAR_1} won by default!$"

gBattleDomeWinText5::
	.string "{STR_VAR_1} won outright by default!$"

gBattleDomeWinText6::
	.string "{STR_VAR_1} won without using a move!$"

gBattleDomeWinText7::
	.string "{STR_VAR_1} won outright with no moves!$"

gBattleDomeMatchNumberText1::
	.string "Round 1, Match 1$"

gBattleDomeMatchNumberText2::
	.string "Round 1, Match 2$"

gBattleDomeMatchNumberText3::
	.string "Round 1, Match 3$"

gBattleDomeMatchNumberText4::
	.string "Round 1, Match 4$"

gBattleDomeMatchNumberText5::
	.string "Round 1, Match 5$"

gBattleDomeMatchNumberText6::
	.string "Round 1, Match 6$"

gBattleDomeMatchNumberText7::
	.string "Round 1, Match 7$"

gBattleDomeMatchNumberText8::
	.string "Round 1, Match 8$"

gBattleDomeMatchNumberText9::
	.string "Round 2, Match 1$"

gBattleDomeMatchNumberText10::
	.string "Round 2, Match 2$"

gBattleDomeMatchNumberText11::
	.string "Round 2, Match 3$"

gBattleDomeMatchNumberText12::
	.string "Round 2, Match 4$"

gBattleDomeMatchNumberText13::
	.string "Semifinal Match 1$"

gBattleDomeMatchNumberText14::
	.string "Semifinal Match 2$"

gBattleDomeMatchNumberText15::
	.string "Final Match$"
